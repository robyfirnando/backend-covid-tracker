<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes(['register' => false]);

Route::group(['middleware'=>'isLoggedIn'], function() {
    Route::get('/dashboard', [
        'uses' => 'DashboardController@index',
        'as' => 'dashboard'
    ]);

    // route admin
    Route::group(['namespace'=>'Backend'], function() {
        Route::resource('place', 'PlaceController')->except(['destroy']);
        Route::get('/place/{id}/destroy', [
            'uses' => 'PlaceController@destroy',
            'as' => 'place.destroy'
        ]);

        Route::resource('user', 'UserController')->except(['destroy']);
        Route::get('/user/{id}/destroy', [
            'uses' => 'UserController@destroy',
            'as' => 'user.destroy'
        ]);

        Route::resource('satgas', 'SatgasController')->except(['destroy']);
        Route::get('/satgas/{id}/destroy', [
            'uses' => 'SatgasController@destroy',
            'as' => 'satgas.destroy'
        ]);

        Route::resource('vaccine', 'VaccineController')->except(['create', 'destroy', 'edit']);
        Route::get('/vaccine/{id}/create', [
            'uses' => 'VaccineController@create',
            'as' => 'vaccine.create'
        ]);
        Route::get('/vaccine/{uid}/{id}/edit', [
            'uses' => 'VaccineController@edit',
            'as' => 'vaccine.edit'
        ]);
        Route::get('/vaccine/{uid}/{id}/destroy', [
            'uses' => 'VaccineController@destroy',
            'as' => 'vaccine.destroy'
        ]);

        Route::resource('test-covid', 'TestCovidController')->except(['update', 'create', 'index', 'edit', 'destroy']);
        Route::group(['prefix'=>'test-covid','as'=>'test-covid.'], function() {
            Route::get('/test-covid/{uid}/index', [
                'uses' => 'TestCovidController@index',
                'as' => 'index'
            ]);
            Route::get('/test-covid/{uid}/create', [
                'uses' => 'TestCovidController@create',
                'as' => 'create'
            ]);
            Route::get('/test-covid/{uid}/{id}/edit', [
                'uses' => 'TestCovidController@edit',
                'as' => 'edit'
            ]);
            Route::put('/test-covid/{uid}/{id}/update', [
                'uses' => 'TestCovidController@update',
                'as' => 'update'
            ]);
            Route::get('/test-covid/{uid}/{id}/destroy', [
                'uses' => 'TestCovidController@destroy',
                'as' => 'destroy'
            ]);
        });

        Route::resource('user-journey', 'UserJourneyController');
        Route::group(['prefix' => 'user-journey', 'as' => 'user-journey.'], function() {
            Route::post('/get-journey', [
                'as' => 'get-journey',
                'uses' => 'UserJourneyController@getJourney'
            ]);
        });
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    });

    // route satgas
    Route::group(['prefix' => 'dashboard-satgas', 'namespace'=>'Satgas', 'as' => 'satgas.'], function() {

        // test covid by satgas
        Route::get('/test-covid/datatable', [
            'as' => 'test-covid.datatable',
            'uses' => 'TestCovidController@datatable'
        ]);
        Route::get('/test-covid/{uid}/{id}/destroy', [
            'as' => 'test-covid.destroy',
            'uses' => 'TestCovidController@destroy'
        ]);
        Route::resource('test-covid', 'TestCovidController')->except(['show', 'edit', 'destroy']);

        Route::get('/vaccine/datatable', [
            'as' => 'vaccine.datatable',
            'uses' => 'VaccineController@datatable'
        ]);
        Route::get('/vaccine/{uid}/{id}/destroy', [
            'as' => 'vaccine.destroy',
            'uses' => 'VaccineController@destroy'
        ]);
        Route::resource('vaccine', 'VaccineController');
    });

    Route::get('get-user', [
        'as' => 'select.get-user',
        'uses' => 'SelectController@getUser'
    ]);
});