<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::group(['prefix'=>'v1', 'middleware' => ['api']], function () {

    Route::group(['prefix' => 'user-journey', 'namespace' => 'Api', 'as' => 'user-journey.'], function () {
        Route::get('get-user-journey', [
            'as' =>'get-user-journey',
            'uses' =>'UserJourneyController@index',
        ]);
        Route::post('post-user-journey', [
            'as' =>'post-user-journey',
            'uses' =>'UserJourneyController@store',
        ]);
    });
});