<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public function scopeName($q, $v)
    {
        return $q->where('name', 'LIKE', $v);
    }

    public function scopeIsAdmin($q)
    {
        return auth()->user()->id_role == 1;
    }

    public function scopeIsSatgas($q)
    {
        return auth()->user()->id_role == 2;
    }
}
