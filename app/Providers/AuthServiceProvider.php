<?php

namespace App\Providers;

use App\Models\Role;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\Response;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('access-admin', function() {
            return Role::isAdmin() ? Response::allow() : Response::deny('Anda tidak berhak mengakses halaman ini.');
        });

        Gate::define('access-satgas', function() {
            return Role::isSatgas() ? Response::allow() : Response::deny('Anda tidak berhak mengakses halaman ini.');
        });
    }
}
