<?php
namespace App\Helpers;
 
use Kreait\Laravel\Firebase\Facades\Firebase;

class FirebaseHelper {

    public function __construct()
    {
        // $this->database = $database;
    }
    
    public static function vaccineCert($uid, $type) {
        
        $database = Firebase::database()->getReference('VaccineCert')->getChild($uid)->getChild($type)->getValue();
        return $database;
    } 
}
