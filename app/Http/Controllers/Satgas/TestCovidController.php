<?php

namespace App\Http\Controllers\Satgas;

use App\Http\Controllers\Controller;
use App\Traits\AjaxTrait;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use Illuminate\Http\Response;

class TestCovidController extends Controller
{
    use AjaxTrait;

    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'TestCovid';
        $this->tbUser = 'Users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->database->getReference($this->tbUser)->getValue();
        $data['users'] = $users;

        return view('satgas.test-covid.index', $data);
    }

    public function datatable(Request $request)
    {
        $user = '';
        $data = [];
        if (!empty($request->user_id)) {
            $user = "/$request->user_id";
            $model = $this->database->getReference($this->tbName.$user)->getValue();
            if (!empty($model)){
                $data = array_values($model) ?? [];
            }
        }

        $dTable = DataTables()->of($data)
        ->addIndexColumn()
        ->rawColumns(['action']);

        return $dTable->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ref = $this->database->getReference($this->tbName."/".$request->user_id);

        try {
            $lastItem = collect($ref->getValue())->sortByDesc('id')->values()->all();
            
            $id = "testcovid1";
            if (!empty($lastItem)) {
                $lastId = str_replace("testcovid", "", $lastItem[0]['id']);
                $id = "testcovid" . ($lastId + 1);
            }
    
            $key = str_replace('c', "C", $id);
            $title = strtoupper($request->type) . " ".($request->positive == 'true' ? "Positif" : "Negatif");
            $postData = [];
            if (!empty($ref->getValue())) {
                foreach ($ref->getValue() as $k => $data) {
                    $postData[$k] = $data;
                }
            }

            $expDate = $request->expired_date;
            $date = str_replace('/', '-', $expDate);
            
            $postData[$key] = [
                'from' => $request->from,
                'id' => $id,
                'link' => $request->link,
                'positive' => $request->positive == "true" ? true : false,
                'title' => $title,
                'type' => $request->type,
                'uid' => $request->user_id,
                'valid_date' => strtotime(date('Y-m-d', strtotime($date))),
            ];
    
            $postRef = $ref->set($postData);
            
            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Saved !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $expDate = $request->expired_date;
            $date = str_replace('/', '-', $expDate);

            $id = str_replace("c", "C", $id);
            $title = strtoupper($request->type) . " ".($request->positive == 'true' ? "Positif" : "Negatif");

            $postData = [
                'from' => $request->from,
                'link' => $request->link,
                'positive' => $request->positive == "true" ? true : false,
                'title' => $title,
                'type' => $request->type,
                'uid' => $request->user_id,
                'valid_date' => strtotime(date('Y-m-d', strtotime($date)))
            ];
            $postRef = $this->database->getReference("{$this->tbName}/{$request->user_id}")->getChild($id)->update($postData);

            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Updated !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid, $id)
    {
        try {
            $id = str_replace("c", "C", $id);
            $postRef = $this->database->getReference("{$this->tbName}/{$uid}")->getChild($id)->remove();

            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Deleted !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }
}
