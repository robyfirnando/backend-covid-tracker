<?php

namespace App\Http\Controllers\Satgas;

use App\Http\Controllers\Controller;
use App\Traits\AjaxTrait;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Response;

class VaccineController extends Controller
{
    use AjaxTrait;

    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'VaccineCert';
        $this->tbUser = 'Users';
        $this->arr = [
            'first_dose' => 'vaccinecert1',
            'second_dose' => 'vaccinecert2',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->database->getReference($this->tbUser)->getValue();
        $data['users'] = $users;

        return view('satgas.vaccine-cert.index', $data);
    }

    public function datatable(Request $request)
    {
        $user = '';
        $data = [];
        if (!empty($request->user_id)) {
            $user = "/$request->user_id";
            $model = $this->database->getReference($this->tbName.$user)->getValue();
            // dd($model);
            if (!empty($model)){
                $data = array_values($model) ?? [];
            }
        }

        $dTable = DataTables()->of($data)
        ->addIndexColumn()
        ->rawColumns(['action', 'image']);

        return $dTable->toJson();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = $request->user_id;

        try {
            $time = time();
            $seq = 1;
            $user = $this->database->getReference($this->tbUser)->getChild($userId)->getValue();
            $row = $this->database->getReference($this->tbName)->getChild($userId);
            $value = [];
            if (!empty($row)) {
                if (!empty($dataVac = $row->getValue())) {
                    $seq = count($dataVac)+1;
                    $value = $dataVac;
                }
            }
    
            $color = '#5A5B6C';
    
            $img = Image::make('https://i.postimg.cc/prVKKFcf/Untitled.png');
            $img->text($user['name'], 215, 150, function($font) use($color) {
                $font->color($color);
            });
            $img->text($user['nik'], 215, 210, function($font) use($color) {
                $font->color($color);
            });
    
            $img->text($userId, 300, 235, function($font) use($color) {
                $font->color($color);
            });
    
            $img->text(date('d M Y'), 375, 265, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->size(12);
                $font->color($color);
            });
    
            $img->text(date('d M Y'), 360, 282, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->size(8);
                $font->color($color);
            });
    
            $img->text(strtoupper($request->vaccine_name ?? "-"), 295, 358, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->align('right');
                $font->size(12);
                $font->color($color);
            });
            $out = public_path("$userId-$time.jpg");
            $img->save($out); 
            
            $value['vaccinecert'.$seq] = [
                'id' => 'vaccinecert'.$seq,
                'seq' => $seq,
                'status' => true,
                'title' => $request->title,
                'vaccine_name' => ucfirst($request->vaccine_name),
                'image' => url("$userId-$time.jpg"),
            ];
            $postRef = $row->set($value);

            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Saved !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uid = $request->user_id;
        try {
            $time = time();
            $user = $this->database->getReference($this->tbUser)->getChild($uid)->getValue();
            $row = $this->database->getReference($this->tbName)->getChild($uid)->getChild($id);

            $color = '#5A5B6C';

            $img = Image::make('https://i.postimg.cc/prVKKFcf/Untitled.png');
            $img->text($user['name'], 215, 150, function($font) use($color) {
                $font->color($color);
            });
            $img->text($user['nik'], 215, 210, function($font) use($color) {
                $font->color($color);
            });

            $img->text($id, 300, 235, function($font) use($color) {
                $font->color($color);
            });

            $img->text(date('d M Y'), 375, 265, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->size(12);
                $font->color($color);
            });

            $img->text(date('d M Y'), 360, 282, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->size(8);
                $font->color($color);
            });

            $img->text(strtoupper($row->getValue()['vaccine_name'] ?? "-"), 295, 358, function($font) use($color) {
                $font->file(public_path('assets/font/arial.ttf'));
                $font->align('right');
                $font->size(12);
                $font->color($color);
            });
            $out = public_path("$id-$time.jpg");
            $img->save($out); 
            
            $value = [];
        /*  if ($row->getValue()['status']) {
                $value['status'] = false;
                $value['vaccine_name'] = "";
                $value['image'] = "";
            } else { */
            $value['title'] = $request->title;
            $value['status'] = true;
            $value['vaccine_name'] = ucfirst($request->vaccine_name);
            $value['image'] = url("$id-$time.jpg");
            // }

            $postRef = $row->update($value);

            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Updated !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid, $id)
    {
        try {
            $postRef = $this->database->getReference("{$this->tbName}/{$uid}")->getChild($id)->remove();

            $this->code = Response::HTTP_OK;
            $this->success = true;
            $this->data = 'Deleted !';
        } catch (\Exception $e) {
            $this->code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->success = false;
            $this->data = 'Server Error: '.$e->getMessage();
        }

        return $this->json();
    }
}
