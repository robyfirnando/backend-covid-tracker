<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserJourney;
use App\Traits\AjaxTrait;
use BaconQrCode\Renderer\Color\Rgb;
use Illuminate\Http\Request;

class UserJourneyController extends Controller
{
    use AjaxTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uj = new UserJourney;
        if (!empty($request->id_user)) {
            $uj = $uj->where('id_user', $request->id_user);
        }
        
        $this->code = 200;
        $this->success = true;
        $this->data = $uj->get();
        
        return $this->json();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $uj = new UserJourney;
            $uj->id_user = $request->id_user;
            $uj->lat = $request->lat;
            $uj->long = $request->long;
            $uj->is_checkin = $request->is_checkin;
            $uj->save();
        } catch (\Exception $e) {
            $this->code = 200;
            $this->success = false;
            $this->data = 'Tyduck OK';
        }

        $this->code = 200;
        $this->success = true;
        $this->data = 'OK';
        
        return $this->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
