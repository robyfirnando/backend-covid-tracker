<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbUser = 'Users';
        $this->tbVaccine = 'VaccineCert';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $ref = $this->database->getReference($this->tbUser)->getValue();
        $data['users'] = $ref;
        return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ref = $this->database->getReference($this->tbUser)->getValue();
        
        $postUser = [
            // 'uid' => 
            'nik' => $request->nik,
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phoneNumber' => $request->phoneNumber,
            'gender' => empty($request->gender) ? 0 : $request->gender
        ];

        $postRef = $this->database->getReference($this->tbUser)->push($postUser)->getKey();
        $postUser['uid'] = $postRef;
        $updateRef = $this->database->getReference($this->tbUser)->getChild($postRef)->update($postUser);

        $postVaccine = [
            'vaccinecert1' => [
                'id' => 'vaccinecert1',
                'image' => '',
                'seq' => 1,
                'status' => false,
                'title' => 'Vaksin Pertama',
                "vaccine_name" => ""
            ],
            'vaccinecert2' => [
                'id' => 'vaccinecert2',
                'image' => '',
                'seq' => 2,
                'status' => false,
                'title' => 'Vaksin Kedua',
                "vaccine_name" => ""
            ],
        ];

        $postVaccines = $this->database->getReference($this->tbVaccine."/".$postRef)->set($postVaccine)->getKey();

        if ($postVaccines) {
            return redirect()->route('user.index')->withSuccess('Success add item !');
        }

        return redirect()->route('user.index')->with('error_msg', 'Failed add item !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ref = $this->database->getReference("{$this->tbUser}/{$id}")
           ->getValue();

        $data['item'] = $ref;
        $data['uid'] = $id;
        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = [
            // 'uid' => 
            'nik' => $request->nik,
            'name' => $request->name,
            'email' => $request->email,
            'phoneNumber' => $request->phoneNumber,
            'gender' => empty($request->gender) ? 0 : $request->gender
        ];
        
        if (!empty($request->password))
            $postData['password'] = $request->password;

        $postRef = $this->database->getReference($this->tbUser)->getChild($id)->update($postData);

        if ($postRef) {
            return redirect()->route('user.index')->withSuccess('Success update item !');
        }

        return redirect()->route('user.index')->with('error_msg', 'Failed update item !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
