<?php

namespace App\Http\Controllers\Backend;

use Alert;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class SatgasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $data['satgas'] = User::role(2)->get();

        return view('admin.satgas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.satgas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $postUser = [
                // 'uid' => 
                'id_role' => Role::name('satgas')->first()->id,
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'instansi' => $request->instansi,
            ];
            if (isset($request->active))
                $postUser['status'] = $request->active;

            User::insert($postUser);
        } catch (\Exception $e) {
            return redirect()->route('satgas.index')->with('error_msg', 'Failed add item !');
        }
        return redirect()->route('satgas.index')->withSuccess('Success add item !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::role(2)->whereId($id)->first();

        $data['item'] = $user;
        $data['id'] = $id;

        return view('admin.satgas.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $postData = [
                // 'uid' => 
                'name' => $request->name,
                'email' => $request->email,
                'instansi' => $request->instansi,
            ];
            
            if (!empty($request->password))
                $postData['password'] = bcrypt($request->password);

            $postData['status'] = 0;
            if (isset($request->active))
                $postData['status'] = $request->active;
    
            $user = User::whereId($id)->update($postData);
            
        } catch (\Exception $e) {
            return redirect()->route('satgas.index')->with('error_msg', 'Failed update item !');
        }

        return redirect()->route('satgas.index')->withSuccess('Success update item !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::find($id)->delete();
        } catch (\Exception $e) {
            return redirect()->route('satgas.index')->with('error_msg', 'Failed delete item !');
        }

        return redirect()->route('satgas.index')->withSuccess('Success delete item !');
    }
}
