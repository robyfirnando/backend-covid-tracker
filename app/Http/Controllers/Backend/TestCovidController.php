<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Arr;

class TestCovidController extends Controller
{
    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'TestCovid';
        $this->tbUser = 'Users';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $ref = $this->database->getReference($this->tbName)->getValue();
        $userRef  = $this->database->getReference($this->tbUser)->getValue();
        
        $output = [];
        if (!empty($ref)) {
            if (!empty($ref[$id])) {
                $output = $ref[$id];
            }
        }

        $data['tests'] = $output;
        $data['user'] = !empty($userRef) ? $userRef[$id] : die('User Kosong !!');

        return view('admin.test-covid.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->route('uid');
        $data['userId'] = $id;

        return view('admin.test-covid.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $ref = $this->database->getReference($this->tbName."/".$request->user_id);

        $lastItem = collect($ref->getValue())->sortByDesc('id')->values()->all();
        
        $id = "testcovid1";
        if (!empty($lastItem)) {
            $lastId = str_replace("testcovid", "", $lastItem[0]['id']);
            $id = "testcovid" . ($lastId + 1);
        }

        $key = str_replace('c', "C", $id);
        $title = strtoupper($request->type) . " ".($request->positive == 'true' ? "Positif" : "Negatif");
        $postData = [];
        if (!empty($ref->getValue())) {
            foreach ($ref->getValue() as $k => $data) {
                $postData[$k] = $data;
            }
        }

        $expDate = $request->expired_date;
        $date = str_replace('/', '-', $expDate);

        $postData[$key] = [
            'from' => $request->from,
            'id' => $id,
            'link' => $request->link,
            'positive' => $request->positive == "true" ? true : false,
            'title' => $title,
            'type' => $request->type,
            'uid' => $request->user_id,
            'valid_date' => strtotime(date('Y-m-d', strtotime($date)))
        ];

        /* if (empty($ref->getValue())) {
            $postData[$request->user_id] = [
                'from' => $request->from,
                'id' => $id,
                'link' => $request->link,
                'positive' => $request->positive,
                'title' => $title,
                'type' => $request->type,
                'uid' => $request->user_id,
                'valid_date' => time()
            ];
        }
        dd($postData); */
        $postRef = $ref->set($postData);

        if ($postRef) {
            return redirect()->route('test-covid.index', $request->user_id)->withSuccess('Success add item !');
        }

        return redirect()->route('test-covid.index', $request->user_id)->with('error_msg', 'Failed add item !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uid, $id)
    {
        $id = str_replace("c", "C", $id);
        $ref = $this->database->getReference("{$this->tbName}/{$uid}/{$id}")
           ->getValue();
        // dd($ref);
        $data['item'] = $ref;
        return view('admin.test-covid.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid, $id)
    {
        // dd($uid, $id);
        $id = str_replace("c", "C", $id);
        $title = strtoupper($request->type) . " ".($request->positive == 'true' ? "Positif" : "Negatif");
        $expDate = $request->expired_date;
        $date = str_replace('/', '-', $expDate);
        // dd($date);
        $postData = [
            'from' => $request->from,
            'link' => $request->link,
            'positive' => $request->positive == "true" ? true : false,
            'title' => $title,
            'type' => $request->type,
            'valid_date' => strtotime(date('Y-m-d', strtotime($date)))
        ];
        $postRef = $this->database->getReference("{$this->tbName}/{$uid}")->getChild($id)->update($postData);

        if ($postRef) {
            return redirect()->route('test-covid.index', $uid)->withSuccess('Success update item !');
        }

        return redirect()->route('test-covid.index', $uid)->with('error_msg', 'Failed update item !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid, $id)
    {
        $id = str_replace("c", "C", $id);
        $postRef = $this->database->getReference("{$this->tbName}/{$uid}")->getChild($id)->remove();

        if ($postRef) {
            return redirect()->route('test-covid.index', $uid)->withSuccess('Success delete item !');
        }

        return redirect()->route('test-covid.index', $uid)->with('error_msg', 'Failed delete item !');
    }
}
