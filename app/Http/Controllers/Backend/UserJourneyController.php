<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\UserJourney;
use App\Traits\AjaxTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Database;
use DB;

class UserJourneyController extends Controller
{
    use AjaxTrait;

    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbUser = 'Users';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->database->getReference($this->tbUser)->getValue();
        $data['users'] = $users;
        $data['journeys'] = UserJourney::all();

        return view('admin.user-journey.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getJourney(Request $request)
    {
        try {
            $data = UserJourney::whereIdUser($request->user_id);
            
            if (!empty($request->start) && $request->end) {
                $sDate = Carbon::createFromFormat('d/m/Y', $request->start)->format('Y-m-d');
                $eDate = Carbon::createFromFormat('d/m/Y', $request->end)->format('Y-m-d');

                $data = $data->whereBetween(DB::raw('DATE(created_at)'), [$sDate, $eDate]);
                
            }

            $data = $data->orderBy('id', 'asc')->get();

            $this->code = 200;
            $this->success = true;
            $this->data = $data;
        } catch (\Exception $e) {
            $this->code = 500;
            $this->success = true;
        }
        return $this->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
