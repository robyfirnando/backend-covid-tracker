<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use RealRashid\SweetAlert\Facades\Alert;
use Intervention\Image\Facades\Image;

class VaccineController extends Controller
{
    private $arr;

    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'VaccineCert';
        $this->tbUser = 'Users';
        $this->arr = [
            'first_dose' => 'vaccinecert1',
            'second_dose' => 'vaccinecert2',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['uid'] = $id;
        return view('admin.vaccine-cert.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $time = time();
        $seq = 1;
        $user = $this->database->getReference($this->tbUser)->getChild($id)->getValue();
        $row = $this->database->getReference($this->tbName)->getChild($id);
        $value = [];
        if (!empty($row)) {
            if (!empty($dataVac = $row->getValue())) {
                $seq = count($dataVac)+1;
                $value = $dataVac;
            }
        }

        $color = '#5A5B6C';

        $img = Image::make('https://i.postimg.cc/prVKKFcf/Untitled.png');
        $img->text($user['name'], 215, 150, function($font) use($color) {
            $font->color($color);
        });
        $img->text($user['nik'], 215, 210, function($font) use($color) {
            $font->color($color);
        });

        $img->text($id, 300, 235, function($font) use($color) {
            $font->color($color);
        });

        $img->text(date('d M Y'), 375, 265, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->size(12);
            $font->color($color);
        });

        $img->text(date('d M Y'), 360, 282, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->size(8);
            $font->color($color);
        });

        $img->text(strtoupper($request->vaccine_name ?? "-"), 295, 358, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->align('right');
            $font->size(12);
            $font->color($color);
        });
        $out = public_path("$id-$time.jpg");
        $img->save($out); 
        
        $value['vaccinecert'.$seq] = [
            'id' => 'vaccinecert'.$seq,
            'seq' => $seq,
            'status' => true,
            'title' => $request->title,
            'vaccine_name' => ucfirst($request->vaccine_name),
            'image' => url("$id-$time.jpg"),
        ];
        $postRef = $row->set($value);
        
        if ($postRef) {
            return redirect()->route('vaccine.show', $id)->withSuccess('Success add item !');
        }

        return redirect()->route('vaccine.show', $id)->with('error_msg', 'Failed add item !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
        
        $row = $this->database->getReference($this->tbName)->getChild($id)->getValue();

        $data['vaccines'] = empty($row) ? [] : $row;
        $data['uid'] = $id;
        return view('admin.vaccine-cert.index', $data);
        // return redirect("/$id-$time.jpg");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uid, $id)
    {
        $data['id'] = $id;
        $data['uid'] = $uid;
        $row = $this->database->getReference($this->tbName)->getChild($uid)->getChild($id);
        $data['item'] = $row->getValue();

        return view('admin.vaccine-cert.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uid = $request->uid;
        $time = time();
        $user = $this->database->getReference($this->tbUser)->getChild($uid)->getValue();
        $row = $this->database->getReference($this->tbName)->getChild($uid)->getChild($id);
        
        $color = '#5A5B6C';

        $img = Image::make('https://i.postimg.cc/prVKKFcf/Untitled.png');
        $img->text($user['name'], 215, 150, function($font) use($color) {
            $font->color($color);
        });
        $img->text($user['nik'], 215, 210, function($font) use($color) {
            $font->color($color);
        });

        $img->text($id, 300, 235, function($font) use($color) {
            $font->color($color);
        });

        $img->text(date('d M Y'), 375, 265, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->size(12);
            $font->color($color);
        });

        $img->text(date('d M Y'), 360, 282, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->size(8);
            $font->color($color);
        });

        $img->text(strtoupper($row->getValue()['vaccine_name'] ?? "-"), 295, 358, function($font) use($color) {
            $font->file(public_path('assets/font/arial.ttf'));
            $font->align('right');
            $font->size(12);
            $font->color($color);
        });
        $out = public_path("$id-$time.jpg");
        $img->save($out); 
        
        $value = [];
       /*  if ($row->getValue()['status']) {
            $value['status'] = false;
            $value['vaccine_name'] = "";
            $value['image'] = "";
        } else { */
        $value['title'] = $request->title;
        $value['status'] = true;
        $value['vaccine_name'] = ucfirst($request->vaccine_name);
        $value['image'] = url("$id-$time.jpg");
        // }
        $postRef = $row->update($value);
        
        if ($postRef) {
            return redirect()->route('vaccine.show', $uid)->withSuccess('Success update item !');
        }

        return redirect()->route('vaccine.show', $uid)->with('error_msg', 'Failed update item !');
        // return redirect()->route('user.index')->with('error_msg', 'Failed update vaccine !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid, $id)
    {
        $postRef = $this->database->getReference("{$this->tbName}/{$uid}")->getChild($id)->remove();
        
        if ($postRef) {
            return redirect()->route('vaccine.show', $uid)->withSuccess('Success delete item !');
        }

        return redirect()->route('vaccine.show', $uid)->with('error_msg', 'Failed delete item !');
    }
}
