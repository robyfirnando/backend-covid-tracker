<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use RealRashid\SweetAlert\Facades\Alert;

class PlaceController extends Controller
{
    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'Places';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $ref = $this->database->getReference($this->tbName)->getValue();
        $data['places'] = $ref;
        return view('admin.places.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.places.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ref = $this->database->getReference($this->tbName)->getValue();
        $lastId = str_replace("places", "", collect($ref)->sortDesc()->values()->all()[0]['id']);
        $id = "places" . ($lastId + 1);
        $postData = [
            'banner' => '', 
            'id' => $id, 
            'lat' => (int)$request->lat, 
            'long' => (int)$request->long, 
            'name' => $request->name, 
            'need_test' => strtolower($request->test), 
            'need_vaccine' => (int)$request->need_vaccine, 
            'quota' => (int)$request->quota, 
        ];

        $postRef = $this->database->getReference($this->tbName)->getChild($id)->set($postData);

        if ($postRef) {
            return redirect()->route('place.index')->withSuccess('Success add item !');
        }

        return redirect()->route('place.index')->with('error_msg', 'Failed add item !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['id'] = $id;
        return view('admin.places.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ref = $this->database->getReference("{$this->tbName}/{$id}")
           ->getValue();
        // dd($ref);
        $data['item'] = $ref;
        return view('admin.places.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $postData = [
            'banner' => '', 
            'id' => $id, 
            'lat' => (float)$request->lat, 
            'long' => (float)$request->long, 
            'name' => $request->name, 
            'need_test' => strtolower($request->test), 
            'need_vaccine' => (int)$request->need_vaccine, 
            'quota' => (int)$request->quota, 
        ];

        $postRef = $this->database->getReference($this->tbName)->getChild($id)->update($postData);

        if ($postRef) {
            return redirect()->route('place.index')->withSuccess('Success update item !');
        }

        return redirect()->route('place.index')->with('error_msg', 'Failed update item !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postRef = $this->database->getReference($this->tbName)->getChild($id)->remove();
        if ($postRef) {
            return redirect()->route('place.index')->withSuccess('Success delete item !');
        }

        return redirect()->route('place.index')->with('error_msg', 'Failed delete item !');
    }
}
