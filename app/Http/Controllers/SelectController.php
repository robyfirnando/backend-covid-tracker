<?php

namespace App\Http\Controllers;

use App\Traits\AjaxTrait;
use Illuminate\Http\Request;
use Kreait\Firebase\Database;

class SelectController extends Controller
{
    use AjaxTrait;

    public function __construct(Database $database) 
    {
        $this->database = $database;
        $this->tbName = 'Users';
    }

    public function getUser()
    {
        $ref = $this->database->getReference($this->tbName)->getValue();
        
        $this->success = true;
        $this->code = \Illuminate\Http\Response::HTTP_OK;
        $this->data = $ref;

        return $this->json();
    }
}
