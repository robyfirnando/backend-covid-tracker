<html>
    <head>
        <style type='text/css'>
            body, html {
                margin: 0;
                padding: 0;
            }
            body {
                color: black;
                display: table;
                font-family: Georgia, serif;
                font-size: 24px;
                text-align: center;
            }
            .container {
                border: 20px solid tan;
                width: 750px;
                height: 563px;
                display: table-cell;
                vertical-align: middle;
            }
            .logo {
                color: tan;
            }

            .marquee {
                color: tan;
                font-size: 48px;
                margin: 20px;
            }
            .assignment {
                margin: 20px;
            }
            .person {
                border-bottom: 2px solid black;
                font-size: 32px;
                font-style: italic;
                margin: 20px auto;
                width: 400px;
            }
            .reason {
            	font-size:20px;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="logo">
                Surat Keterangan Vaksinasi COVID-19
            </div>

            <div class="marquee">
                Sertifikat Vaksin
            </div>

            <div class="assignment">
                Sertifikat ini diberikan kepada
            </div>

            <div class="person">
                {{ $user['name'] }}
            </div>
            <div style="font-size:12px;">NIK : {{ $user['nik'] }} | ID : {{ $user['uid'] }}</div>
            <div style="font-size:15px;">telah dilakukan vaksinasi COVID-19 untuk dosis {{ $user['dosis'] }}</div>

            <div class="reason">
                Sesuai dengan Peraturan Menteri Kesehatan Republik Indonesia
            </div>
        </div>
    </body>
</html>