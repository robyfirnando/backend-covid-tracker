@extends('layouts.template')

@section('userMenu', 'active')

@section('content')
<div class="main-panel">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                    <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                    <i class="material-icons visible-on-sidebar-mini">view_list</i>
                </button>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"> List Users </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">dashboard</i>
                            <p class="hidden-lg hidden-md">Dashboard</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">notifications</i>
                            <span class="notification">5</span>
                            <p class="hidden-lg hidden-md">
                                Notifications
                                <b class="caret"></b>
                            </p>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Mike John responded to your email</a>
                            </li>
                            <li>
                                <a href="#">You have 5 new tasks</a>
                            </li>
                            <li>
                                <a href="#">You're now friend with Andrew</a>
                            </li>
                            <li>
                                <a href="#">Another Notification</a>
                            </li>
                            <li>
                                <a href="#">Another One</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">person</i>
                            <p class="hidden-lg hidden-md">Profile</p>
                        </a>
                    </li>
                    <li class="separator hidden-lg hidden-md"></li>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group form-search is-empty">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="material-input"></span>
                    </div>
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </form>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <a href="{{ route('test-covid.create', $user['uid']) }}">
                            <div class="card-header card-header-icon" data-background-color="purple">
                                <i class="material-icons">add</i>
                            </div>
                        </a>
                        <div class="card-content">
                            <h4 class="card-title">List Test Covid By {{ !empty($user) ? $user['name'] : "KOSONG !" }}</h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table class="datatables table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                            <th>Hospital</th>
                                            <th>Date</th>
                                            <th>Link</th>
                                            <th>Type Test</th>
                                            <th>Result</th>
                                            <th class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Hospital</th>
                                            <th>Date</th>
                                            <th>Link</th>
                                            <th>Type Test</th>
                                            <th>Result</th>
                                            <th class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @forelse ($tests as $test)
                                            <tr>
                                                <td>{{ $test['from'] }}</td>
                                                <td>{{ date('Y-m-d H:i:s', $test['valid_date']) }}</td>
                                                <td>{{ $test['link'] }}</td>
                                                <td>{{ strtoupper($test['type'] ?? "") }}</td>
                                                <td>{{ $test['positive'] == "true" ? "Positif" : "Negatif" }}</td>
                                                <td class="td-actions text-right">
                                                    <!-- <a href="{{ route('user.edit', $test['uid'] ?? "") }}" type="button" rel="tooltip" class="btn btn-sm btn-success btn-round">
                                                        <i class="material-icons">add</i>
                                                    </a>
                                                    <-->
                                                    <a href="{{ route('test-covid.edit', ['uid' => $user['uid'], 'id' => $test['id'] ?? ""]) }}" type="button" rel="tooltip" class="btn btn-sm btn-info btn-round">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <a onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('test-covid.destroy', ['uid' => $user['uid'], 'id' => $test['id'] ?? ""]) }}" rel="tooltip" class="btn btn-sm btn-danger btn-round">
                                                        <i class="material-icons">close</i>
                                                    </a> 
                                                </td>
                                            </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
@endsection