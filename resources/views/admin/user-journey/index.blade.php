@extends('layouts.template')

@section('journeyMenu', 'active')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin="" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.5/leaflet-routing-machine.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/perliedman-leaflet-control-geocoder/1.5.5/Control.Geocoder.min.css">

<style>
    #mapid>div.leaflet-control-container>div.leaflet-top.leaflet-right {
        display: none !important;
    }
</style>
@endsection

@section('content')
<div class="main-panel">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                    <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                    <i class="material-icons visible-on-sidebar-mini">view_list</i>
                </button>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"> User Journey </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">dashboard</i>
                            <p class="hidden-lg hidden-md">Dashboard</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">notifications</i>
                            <span class="notification">5</span>
                            <p class="hidden-lg hidden-md">
                                Notifications
                                <b class="caret"></b>
                            </p>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Mike John responded to your email</a>
                            </li>
                            <li>
                                <a href="#">You have 5 new tasks</a>
                            </li>
                            <li>
                                <a href="#">You're now friend with Andrew</a>
                            </li>
                            <li>
                                <a href="#">Another Notification</a>
                            </li>
                            <li>
                                <a href="#">Another One</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">person</i>
                            <p class="hidden-lg hidden-md">Profile</p>
                        </a>
                    </li>
                    <li class="separator hidden-lg hidden-md"></li>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group form-search is-empty">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="material-input"></span>
                    </div>
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </form>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h4 class="card-title">Satellite Map</h4>
                        </div>
                        <div class="card-content">
                            <div class="row col-md-12">
                                <div class="col-md-4 col-sm-12 mb-0">
                                    <select class="form-control select-user text-black" id="sel-user-filter" style="width:100%">
                                        <option></option>
                                        @forelse ($users as $key => $user)
                                        <option value="{{ $key }}"> {{ $user['name'] }} </option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                                <div class="col-md-2 pt-2 px-0 mb-0">
                                    <input type="text" class="form-control datepicker date-start" value="" placeholder="Date Start">
                                </div>
                                <div class="col-md-2 pt-2 px-0 mb-0">
                                    <input type="text" class="form-control datepicker date-end" value="" placeholder="Date End">
                                </div>
                                <div class="col-md-4 pt-2 px-0 mb-0">
                                    <!-- <button class="btn btn-primary mt-4" id="btn-filter">Filter</button> -->
                                    <a class="btn btn btn-secondary mt-4" href="javascript:window.location.href=window.location.href">Reset</a>
                                </div>
                            </div>
                            <div class="row col-md-12" style="padding-bottom:30px !important;">
                                <div style="height: 500px;" id="mapid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">room</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Custom Skin & Settings Map</h4>
                            <div id="customSkinMap" class="map"></div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.5/leaflet-routing-machine.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/perliedman-leaflet-control-geocoder/1.5.5/Control.Geocoder.min.js"></script>

<script>
    let journeys = '{!! $journeys !!}'
    var mymap = L.map('mapid').setView([-7.8956284, 112.5364175, 15], 13);

    let arrWay = []
    // $.each(JSON.parse(journeys), function(i, v) {
    //     arrWay.push(L.latLng(v.lat, v.long))
    // })
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap);
    var control = L.Routing.control({
        waypoints: arrWay,
        routeWhileDragging: false,
        draggableWaypoints: false,
        altLineOptions: {
            styles: [
                {color: 'black', opacity: 0.15, weight: 9},
                {color: 'white', opacity: 0.8, weight: 6},
                {color: 'blue', opacity: 0.5, weight: 2}
            ]
        },
        lineOptions : {
            addWaypoints: false
        },
        // createMarker: ,
        // createMarker: function(i, wp, nWps) {
        //     var greenIcon = new L.Icon({
        //         iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Red_Circle%28small%29.svg/2048px-Red_Circle%28small%29.svg.png',
        //         // shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/images/marker-shadow.png',
        //         iconSize: [25, 41],
        //         iconAnchor: [12, 41],
        //         popupAnchor: [1, -34],
        //         shadowSize: [41, 41]
        //     });
        //     // here change all the others
        //     return L.marker(wp.latLng, {
        //         icon: yourOtherCustomIconInstance
        //     })
        // },
        router: new L.Routing.osrmv1({
            language: 'en',
            profile: 'car'
        }),
        geocoder: L.Control.Geocoder.nominatim({})
    }).addTo(mymap);

    $('.select-user').select2({
        placeholder: 'Choose User'
    })
    
    $('.select-user, .datepicker').on('change dp.change', function(e) {
        let userId = $('.select-user').val()
        if (userId == "") {
            alert('Please select User ID !')
            $('.datepicker').val("")
            return
        }
        let dateStart = $('.date-start').val()
        let dateEnd = $('.date-end').val()

        $.ajax({
            type: "POST",
            url: "{{ route('user-journey.get-journey') }}",
            data: { user_id: userId, start: dateStart, end: dateEnd },
            dataType: "json",
            success: function (res) {
                let data = res.data
                let arrWay = []

                control.setWaypoints(arrWay)
                let c = data.length - 1
                
                $.each(data, function(i, v) {
                    arrWay.push(L.latLng(v.lat, v.long))
                })

                waypoint(arrWay, data)
                // console.log(arrWay, data)
                control.setWaypoints(arrWay)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    })

    function waypoint(arrAway = [], data = []) {
        control = L.Routing.control({
            waypoints: arrWay,
            routeWhileDragging: false,
            draggableWaypoints: false,
            altLineOptions: {
                styles: [
                    {color: 'black', opacity: 0.15, weight: 9},
                    {color: 'white', opacity: 0.8, weight: 6},
                    {color: 'blue', opacity: 0.5, weight: 2}
                ]
            },
            lineOptions : {
                addWaypoints: false
            },
            createMarker: function (i, waypoint, n) {
                let icon = L.icon({
                    iconUrl: 'https://static.thenounproject.com/png/331569-200.png',
                    iconSize: [20, 20],
                });
                let marker = L.marker(waypoint.latLng);
                let targetIcon = new L.Icon({
                    iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Red_Circle%28small%29.svg/2048px-Red_Circle%28small%29.svg.png',
                    // shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/images/marker-shadow.png',
                    iconSize: [10, 10]
                });
                $.each(data, function(idx, v) {
                    
                    if (waypoint.latLng.lat == v.lat && waypoint.latLng.lng == v.long) {
                        if (v.is_checkin == 1) {
                            marker = L.marker(waypoint.latLng, {
                                bounceOnAdd: false,
                                bounceOnAddOptions: {
                                    duration: 1000,
                                    height: 800,
                                    function() {
                                        (bindPopup(myPopup).openOn(map))
                                    }
                                },
                                icon: L.icon({
                                    iconUrl: 'https://www.pngkey.com/png/full/236-2364299_google-maps-orange-marker.png',
                                    iconSize: [25, 40],
                                    iconAnchor: [22, 94],
                                    popupAnchor: [-3, -76],
                                    shadowSize: [68, 95],
                                    shadowAnchor: [22, 94]
                                })
                            });
                        } else {
                            // here change all the others
                            marker = L.marker(waypoint.latLng, {
                                icon: targetIcon
                            })
                        }
                    }
                })
                return marker;
            },
            router: new L.Routing.osrmv1({
                language: 'en',
                profile: 'car'
            }),
            geocoder: L.Control.Geocoder.nominatim({})
        }).addTo(mymap);
    }
</script>
@endsection