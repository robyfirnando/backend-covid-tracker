<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="text-color:black !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:black;">Form Vaccine</h5>
            </div>
            <form class="form-vaccine" action="">
                <div class="modal-body">
                    @csrf
                    <div id="method-canvas"></div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Users</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <select id="user_id" name="user_id" class="form-control select-user text-black" style="width:100%;">
                                    @forelse ($users as $key => $user)
                                    <option value="{{ $key }}"> {{ $user['name'] }} </option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Title</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" placeholder="Name" name="title" id="title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Vaccine Name</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" placeholder="Vaccine Name" name="vaccine_name" id="vaccine_name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>