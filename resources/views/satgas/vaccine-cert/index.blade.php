@extends('layouts.template')

@section('VaccineMenu', 'active')

@section('content')
<div class="main-panel">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                    <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                    <i class="material-icons visible-on-sidebar-mini">view_list</i>
                </button>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"> Vaccine </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">dashboard</i>
                            <p class="hidden-lg hidden-md">Dashboard</p>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">notifications</i>
                            <span class="notification">5</span>
                            <p class="hidden-lg hidden-md">
                                Notifications
                                <b class="caret"></b>
                            </p>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Mike John responded to your email</a>
                            </li>
                            <li>
                                <a href="#">You have 5 new tasks</a>
                            </li>
                            <li>
                                <a href="#">You're now friend with Andrew</a>
                            </li>
                            <li>
                                <a href="#">Another Notification</a>
                            </li>
                            <li>
                                <a href="#">Another One</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">person</i>
                            <p class="hidden-lg hidden-md">Profile</p>
                        </a>
                    </li>
                    <li class="separator hidden-lg hidden-md"></li>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group form-search is-empty">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="material-input"></span>
                    </div>
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </form>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <a href="#" onclick="return false;">
                            <div class="card-header card-header-icon btn-add" data-background-color="purple">
                                <i class="material-icons">add</i>
                            </div>
                        </a>

                        <div class="card-content">
                            <h4 class="card-title">Vaccine</h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <div class="row col-md-12">
                                    <div class="col-md-4 col-sm-12 mb-0">
                                    <select class="form-control select-user text-black" id="sel-user-filter" style="width:100%">
                                        <option></option>
                                        @forelse ($users as $key => $user)
                                        <option value="{{ $key }}"> {{ $user['name'] }} </option>
                                        @empty
                                        @endforelse
                                    </select>
                                    </div>
                                    <div class="col-md-4 pt-2 px-0 mb-0">
                                        <!-- <button class="btn btn-primary mt-4" id="btn-filter">Filter</button> -->
                                        <a class="btn btn btn-secondary mt-4" href="javascript:window.location.href=window.location.href">Reset</a>
                                    </div>
                                </div>
                                <table id="dtable-vaccine" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Vaccine Name</th>
                                            <th class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Vaccine Name</th>
                                            <th class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
</div>

@include('satgas.vaccine-cert.modal')
@endsection


@section('script')
@include('layouts.firebase')

<script>
    $('.select-user').select2({
        placeholder: 'Choose User'
    })

    dtable($('#dtable-vaccine'))

    $('#sel-user-filter').change(function() {
        dtable($('#dtable-vaccine'))
    })

    $('.btn-add').click(function(e) {
        e.preventDefault()
        
        $('#modalForm').find('form').attr('action', "{{ route('satgas.vaccine.store') }}")
        $('#modalForm').modal('show')
    })

    $('.form-vaccine').on('submit', function(e) {
        e.preventDefault()
        let formData = new FormData(this);

        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
                swal.fire({
                    title: 'Requesting to Server, Please wait ...',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    didOpen: () => {
                        swal.showLoading();
                    }
                })
            },
            success: function(res) {
                swal.close()
                if (res.success)
                    swal.fire('Berhasil', res.data, 'success')
                else
                    swal.fire('Gagal', res.data, 'error')
        
                $('#modalForm').modal('hide')
                dtable($('#dtable-vaccine'))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    })

    $('#modalForm').on('hidden.bs.modal', function () {
        $('.form-vaccine')[0].reset()
        $('#method-canvas').html('')
    })

    $(document).on('click', '.btn-edit', function(e) {
        e.preventDefault()
        
        let id = $(this).data('id')
        let uid =  $('#sel-user-filter').val()
        let image = $(this).data('image')
        let title = $(this).data('title')
        let vaccineName = $(this).data('vaccinename')

        $('#method-canvas').html('@method("PUT")')
        $('#modalForm').find('form').attr('action', `/dashboard-satgas/vaccine/${id}`)

        $('#user_id').val(uid)
        $('#title').val(title)
        $('#vaccine_name').val(vaccineName)

        $('#modalForm').modal('show')
    })

    $(document).on('click', '.btn-delete', function(e) {
        e.preventDefault()
        let id = $(this).data('id')
        let uid = $('#sel-user-filter').val()

        if (confirm('Are you sure to delete this item ? ')) {
            $.ajax({
                type: "GET",
                url: `/dashboard-satgas/vaccine/${uid}/${id}/destroy`,
                dataType: "json",
                beforeSend: function() {
                    swal.fire({
                        title: 'Requesting to Server, Please wait ...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            swal.showLoading();
                        }
                    })
                },
                success: function (res) {
                    swal.close()
                    if (res.success)
                        swal.fire('Berhasil', res.data, 'success')
                    else
                        swal.fire('Gagal', res.data, 'error')
            
                    dtable($('#dtable-vaccine'))
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
    });

    function dtable(el) {
        if (!$.fn.DataTable.isDataTable(el)) {
            table = el.DataTable({
                processing: true,
                'language': {
                    'loadingRecords': '&nbsp;',
                    'processing': '<div><i class="fa fa-spinner fa-spin"></i></div>'
                },
                "scrollX": true,
                "bAutoWidth": false,
                serverSide: true,
                "lengthMenu": [
                    [20, 50, 100, -1],
                    [20, 50, 100, "All"]
                ],
                "ajax": {
                    url: '{{ route("satgas.vaccine.datatable") }}',
                    data: function(data) {
                        data.user_id = $('#sel-user-filter').val()
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        bSearchable: false
                    },
                    {
                        data: 'image',
                        name: 'image',
                        render: function(data, type, row, meta) {
                            console.log('row', row)
                            return `<img src="${row.image}" style="width:100px;">`
                        },
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'vaccine_name',
                        name: 'vaccine_name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        bSearchable: false,
                        className: 'text-right',
                        render: function(data, type, row, meta) {
                            return `
                            <button type="button" rel="tooltip" class="btn btn-sm btn-info btn-round btn-edit" 
                                data-id="${row.id}"
                                data-vaccinename="${row.vaccine_name}"
                                data-title="${row.title}"
                                data-image="${row.image}"
                            >
                                <i class="material-icons">edit</i>
                            </button>
                            <button data-id="${row.id}" data-uid="${row.uid}" rel="tooltip" class="btn btn-sm btn-danger btn-round btn-delete" data-id="${row.uid}">
                                <i class="material-icons">close</i>
                            </button> 
                            `
                        },
                    },
                ],
                "drawCallback": function(settings) {

                }
            });
        }
        el.DataTable().ajax.reload();
    }
</script>
@endsection