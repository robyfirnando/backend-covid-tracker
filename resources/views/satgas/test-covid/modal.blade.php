<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="text-color:black !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="color:black;">Form Test Covid 19</h5>
            </div>
            <form class="form-test-covid" action="">
                <div class="modal-body">
                    @csrf
                    <div id="method-canvas"></div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Users</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <select id="user_id" name="user_id" class="form-control select-user text-black" style="width:100%;">
                                    @forelse ($users as $key => $user)
                                    <option value="{{ $key }}"> {{ $user['name'] }} </option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Hospital</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input type="text" class="form-control" placeholder="Hospital" id="from" name="from">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Link</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input type="text" class="form-control" placeholder="Link" id="link" name="link">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Type Test</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <select id="type" name="type" class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7">
                                    <option disabled selected>Choose Type Test</option>
                                    <option value="swab">Swab</option>
                                    <option value="pcr">PCR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Result Test</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <select id="positive" name="positive" class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7">
                                    <option disabled selected>Choose Result Test</option>
                                    <option value="true">Positive</option>
                                    <option value="false">Negatif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 label-on-left">Expired Date</label>
                        <div class="col-sm-8">
                            <div class="form-group label-floating is-empty">
                                <input type="text" class="form-control datepicker" id="expired_date" name="expired_date" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>