<div class="sidebar" data-active-color="purple">
    <div class="logo">
        <a href="#" class="simple-text">
            Backend Covid Tracker
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="#" class="simple-text">
            Ct
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="/assets/img/faces/avatar.jpg" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    Admin Covid Tracker
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <!-- <li>
                            <a href="#">My Profile</a>
                        </li>
                        <li>
                            <a href="#">Edit Profile</a>
                        </li> -->
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Menu Admin -->
        @can('access-admin')
            <ul class="nav">
                <li class="@yield('dashboardMenu')" >
                    <a href="/dashboard">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="@yield('placeMenu')">
                    <a href="{{ route('place.index') }}">
                        <i class="material-icons">apartment</i>
                        <p>Places</p>
                    </a>
                </li>
                <li class="@yield('userMenu')">
                    <a href="{{ route('user.index') }}">
                        <i class="material-icons">person</i>
                        <p>Users</p>
                    </a>
                </li>
                <li class="@yield('satgasMenu')">
                    <a href="{{ route('satgas.index') }}">
                        <i class="material-icons">group</i>
                        <p>Satgas</p>
                    </a>
                </li>
                <li class="@yield('journeyMenu')">
                    <a href="{{ route('user-journey.index') }}">
                        <i class="material-icons">explore</i>
                        <p>User Journey</p>
                    </a>
                </li>
            </ul>
        @endcan

        @can('access-satgas')
            <ul class="nav">
                <li class="@yield('dashboardMenu')" >
                    <a href="/dashboard">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="@yield('TestCovidMenu')">
                    <a href="{{ route('satgas.test-covid.index') }}">
                        <i class="material-icons">group</i>
                        <p>Test Covid</p>
                    </a>
                </li>
                <li class="@yield('VaccineMenu')">
                    <a href="{{ route('satgas.vaccine.index') }}">
                        <i class="material-icons">group</i>
                        <p>Vaccine</p>
                    </a>
                </li>
            </ul>
        @endcan
    </div>
</div>