<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $arr = [];
        $arr = [
            [
                'id_role' => 1,
                'name' => 'Nando',
                'email' => 'admin@gmail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => bcrypt('admin'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ];
        User::insert($arr);
    }
}
